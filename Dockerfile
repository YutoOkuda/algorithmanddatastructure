FROM alpine:latest
RUN apk add --no-cache gcc gdb libc-dev alpine-sdk

CMD ["/bin/ash"]
