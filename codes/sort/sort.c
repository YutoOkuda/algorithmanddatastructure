#include "sort.h"

int main() {
    int a[10] = {20, 6, 55, 74, 3, 45, 13, 87, 46, 30};
    int aLenght = sizeof(a) / sizeof(int);

    printArray(a, aLenght);

    // bubbleSort(a, aLenght);
    selectionSort(a, aLenght);

    printArray(a, aLenght);

    return 0;
}

void printArray(int* a, int n) {
    for (int i = 0; i < n; i++) printf("%d\t", a[i]);

    printf("\n");
}

void bubbleSort(int* a, int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = n - 1; j > i; j--) {
            if (a[j - 1] > a[j]) {
                int tmp = a[j - 1];
                a[j - 1] = a[j];
                a[j] = tmp;
            }
        }
    }
}

void selectionSort(int* a, int n) {
    for (int i = 0; i < n - 1; i++) {
        int lowest = i;

        for (int j = i + 1; j < n; j++)
            if (a[j] < a[lowest]) lowest = j;

        int tmp = a[i];
        a[i] = a[lowest];
        a[lowest] = tmp;
    }
}