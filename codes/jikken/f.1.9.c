#include <stdio.h>

int main() {
    // 成績処理結果ファイル読み込み
    FILE *seisekiIn = fopen("seisekiOut.dat", "r");

    // 成績データ格納用2次元配列
    int seiseki[5][19] = {};

    // 一時格納用バッファ
    int buff = 0;
    // 成績カウンタ
    int cnt = 0;
    // 受験番号カウンタ
    int examNum = 1;

    // 成績データを一つずつファイルの終わりまで読み込む
    while (fscanf(seisekiIn, "%d", &buff) != EOF) {
        // 成績データを配列に格納
        seiseki[examNum][cnt] = buff;
        // 成績カウンタを1増やす
        cnt++;

        // 一人分の成績(19回)読んだら受験番号を増やす
        if (cnt > 18) {
            cnt = 0;
            examNum++;
        }
    }

    // ユーザーに受験番号を入力させる
    printf("受験番号を入力してください\n");
    scanf("%d", &examNum);

    // 受験番号に応じた成績を表示
    printf(
        "受験番号\t国語の点数\t数学の点数\t英語の点数\t国語の成績\t社会の成績\t"
        "数学の成績\t理科の成績\t音楽の成績\t美術の成績\t保健体育の成績\t技術家"
        "庭の成績\t英語の成績\t素点\t記録点\t記録換算点\t総合点\t合否\t順位\n");
    for (int i = 1; i < 5; i++)
        if (examNum == seiseki[i][0])
            for (int j = 0; j < 19; j++) printf("%d\t", seiseki[i][j]);

    printf("\n");

    // ファイルを閉じる
    fclose(seisekiIn);

    return 0;
}