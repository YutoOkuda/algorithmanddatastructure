#include <stdio.h>
#include <stdlib.h>

int main() {
    // ファイル開く
    FILE *file = fopen("score", "r");
    // 点数格納用
    int scores[3][3] = {{}, {}, {}};

    // 3行分繰り返し
    for (int i = 0; i < 3; i++) {
        // 3教科分繰り返し
        for (int j = 0; j < 3; j++) {
            // 点数を読み出し
            fscanf(file, "%d", &scores[i][j]);
        }
    }

    // 国語の点数が大きい順にソート
    // 配列の要素数
    int n = sizeof(scores) / sizeof(int *);

    // バブルソートを降順でする
    for (int i = 0; i < n - 1; i++) {
        for (int j = n - 1; j > i; j--) {
            if (scores[j][0] > scores[j - 1][0]) {
                int tmp = scores[j][0];
                scores[j][0] = scores[j - 1][0];
                scores[j - 1][0] = tmp;
            }
        }
    }

    // 書き込み用ファイルを書き込みモードで開く
    FILE *resultFile = fopen("result1", "w");

    // 3行分繰り返し
    for (int i = 0; i < 3; i++) {
        // ファイルに書き込み
        fprintf(resultFile, "%d\n", scores[i][0]);
    }

    // ファイル閉じる
    fclose(file);
    fclose(resultFile);

    return 0;
}