#include <stdio.h>
#include <stdlib.h>

int main() {
    // 成績ファイル
    FILE *fileSeiseki = fopen("seiseki.dat", "r");
    // ソート済み合計点数ファイル
    FILE *fileSorted = fopen("sort.dat", "w");
    // ソート済み成績ファイル
    FILE *fileSeisekiSorted = fopen("seisekiSort.dat", "w");

    // 成績格納用配列 動的確保する
    double **seiseki;
    seiseki = (double **)malloc(sizeof(double *));
    seiseki[0] = (double *)malloc(sizeof(double) * 19);

    // 一時格納用のバッファ
    double buff = 0;
    // 成績1セットカウンタ
    int cnt = 0;
    // 受験番号カウンタ
    int examNum = 1;

    // 成績1つ読み込み 点数バイアスの関係でdouble
    while (fscanf(fileSeiseki, "%lf", &buff) != EOF) {
        // 成績位置カウントが0なら、すなわち新たな成績データが入ってきたら配列の拡張
        if (cnt == 0) {
            seiseki =
                (double **)realloc(seiseki, sizeof(double *) * (examNum + 1));
            seiseki[examNum] = (double *)malloc(sizeof(double) * 19);
        }

        // 受験番号・成績位置カウントをindexにしてデータを格納
        seiseki[examNum][cnt] = buff;
        // 成績位置カウントを増やす
        cnt++;

        // 成績1セット分を入力しおわったら受験番号を増やす
        if (cnt > 12) {
            // 受験時の点数の合計を計算
            for (int i = 1; i < 4; i++)
                seiseki[examNum][13] += seiseki[examNum][i];

            for (int i = 4; i < 13; i++) {
                // 5段階評価の点数の合計
                seiseki[examNum][14] += seiseki[examNum][i];
                // 理数バイアスありの5段階評価の合計
                seiseki[examNum][15] += (i == 5 || i == 6)
                                            ? (seiseki[examNum][i] * 1.4)
                                            : seiseki[examNum][i];
            }

            // 上の700点換算
            seiseki[examNum][14] *= (700.0 / 49.0);

            // テストの点数と上の700点満点の点数を1000点満点で合計
            seiseki[examNum][16] = seiseki[examNum][13] + seiseki[examNum][15];

            // 受験番号を増やして成績位置カウントを0に戻す
            examNum++;
            cnt = 0;
        }
    }
    fclose(fileSeiseki);

    // 成績順でソート
    // バブルソートを降順でする
    for (int i = 1; i < examNum; i++) {
        for (int j = examNum - 1; j > i; j--) {
            if (seiseki[j][15] > seiseki[j - 1][15]) {
                for (int k = 0; k < 16; k++) {
                    double tmp = seiseki[j][k];
                    seiseki[j][k] = seiseki[j - 1][k];
                    seiseki[j - 1][k] = tmp;
                }
            }
        }
    }

    // ソートされた成績をファイルに書き出し
    // 人数分繰り返し
    for (int i = 1; i < examNum; i++) {
        // データの分だけ繰り返し
        for (int j = 0; j < 16; j++) {
            // ファイルに書き込み
            fprintf(fileSorted, "%.0f\t", seiseki[i][j]);
        }
        // 改行書き込み
        fprintf(fileSorted, "\n");
    }
    fclose(fileSorted);

    // 平均値を計算
    double avg = 0;
    for (int i = 1; i < examNum; i++) avg += seiseki[i][15];

    avg /= (examNum - 1);

    // 平均値より合計点が高ければ合格とする
    // 同時に順位も入力決定する
    for (int i = 1; i < examNum; i++) {
        seiseki[i][17] = i;
        if (avg > seiseki[i][15])
            seiseki[i][16] = 0;

        else
            seiseki[i][16] = 1;
    }

    // ソート済み・合否・順位を含む成績ファイルを出力
    // 人数分繰り返し
    for (int i = 1; i < examNum; i++) {
        // データの分だけ繰り返し
        for (int j = 0; j < 18; j++) {
            // ファイルに書き込み
            fprintf(fileSeisekiSorted, "%.0f\t", seiseki[i][j]);
        }
        // 改行書き込み
        fprintf(fileSeisekiSorted, "\n");
    }
    fclose(fileSeisekiSorted);

    // メモリ開放
    for (int i = 0; i < examNum; i++) free(seiseki[i]);
    free(seiseki);

    return 0;
}

/*
受験時の国語・数学・英語の点数（それぞれ100点満点）
国社数理音美保技英の5段階評価

配列のindexと受験番号を対応させる
2次元配列 [受験番号][0-2: 受験時の点数, 3-11: 5段階評価, 12: 受験時の点数の合計,
13: 5段階評価の合計, 14:
理数バイアス(x1.4)した5段階評価の合計を700点満点に換算した点数, 15:
12と14の合計点, 16: 合否(合格: 1, 不合格: 0), 17: 順位] 平均点以上が合格とする
同点の場合は受験番号が若い方が順位が上

受験番号を入力したら成績を表示

ファイルからデータの読み込み
↓
受験人数だけ繰り返す
↓
国数英の合計点・5段階評価の合計点を計算して配列に格納
↓
バイアスつきの5段階評価の合計点を計算して配列に格納
↓
12と14の合計点を配列に格納
↓
繰り返し終わり
↓
合計点で高い順にソート
↓
sort.datに合計点を出力
↓
平均値を計算
↓
合否判定
↓
ソート済みののデータをファイルに出力

ソート済みファイルを配列として読み込む
↓
入力された受験番号をindexにして要素を抽出して表示
*/
