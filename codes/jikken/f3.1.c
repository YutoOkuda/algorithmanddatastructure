#include <stdio.h>
#include <stdlib.h>

int main() {
    // ファイル開く
    FILE *file = fopen("score", "r");
    // 点数格納用
    int score = 0;

    // 3行分繰り返し
    for (int i = 0; i < 3; i++) {
        // 3教科分繰り返し
        for (int j = 0; j < 3; j++) {
            // 点数を読み出し
            fscanf(file, "%d", &score);
            // 点数を表示
            printf("%d\t", score);
        }
        // 改行
        printf("\n");
    }

    // ファイル閉じる
    fclose(file);

    return 0;
}