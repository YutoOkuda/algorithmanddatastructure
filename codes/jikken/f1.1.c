#include <stdio.h>
#include <stdlib.h>

int main() {
    // 読み込む成績ファイル
    FILE *seisekiIn = fopen("seiseki.dat", "r");
    // 書き出す成績ファイル
    FILE *seisekiOut = fopen("seisekiOut.dat", "w");

    // 一時格納用バッファ
    int buff = 0;
    // 成績カウンタ
    int cnt = 0;

    // 成績データを一つずつ読み込む
    while (fscanf(seisekiIn, "%d", &buff) != EOF) {
        // 成績データをファイルに書き込む
        fprintf(seisekiOut, "%d\t", buff);
        // 成績カウンタを1増やす
        cnt++;

        // 一人分の成績(13回)書き込んだら改行
        if (cnt > 12) {
            fprintf(seisekiOut, "\n");
            cnt = 0;
        }
    }

    // ファイルを閉じる
    fclose(seisekiIn);
    fclose(seisekiOut);

    return 0;
}