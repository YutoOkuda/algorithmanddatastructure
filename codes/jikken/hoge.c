#include <stdio.h>
#include <stdlib.h>

int main() {
    double **hoge;
    hoge = (double **)malloc(sizeof(double *) * 6);
    for (int i = 0; i < 6; i++) hoge[i] = (double *)malloc(sizeof(double) * 6);

    for (int i = 0; i < 6; i++)
        for (int j = 0; j < 6; j++) hoge[i][j] = i * j;

    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 6; j++) printf("%.f\t", hoge[i][j]);

        printf("\n");
    }

    hoge = (double **)realloc(hoge, sizeof(double *) * 7);
    for (int i = 0; i < 7; i++)
        hoge[i] = (double *)realloc(hoge[i], sizeof(double) * 7);

    for (int i = 0; i < 6; i++) hoge[i][6] = 123;

    for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 7; j++) printf("%.f\t", hoge[i][j]);

        printf("\n");
    }
    for (int i = 0; i < 7; i++) free(hoge[i]);

    free(hoge);

    return 0;
}