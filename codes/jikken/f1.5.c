#include <stdio.h>
#include <stdlib.h>

int main() {
    // 読み込む成績ファイル
    FILE *seisekiIn = fopen("seiseki.dat", "r");
    // 書き出す成績ファイル
    FILE *seisekiOut = fopen("seisekiOut.dat", "w");

    // 成績データ格納用2次元配列
    double seiseki[5][19] = {};

    // 一時格納用バッファ
    double buff = 0;
    // 成績カウンタ
    int cnt = 0;
    // 受験番号カウンタ
    int examNum = 1;

    // 成績データを一つずつファイルの終わりまで読み込む
    while (fscanf(seisekiIn, "%lf", &buff) != EOF) {
        // 成績データを配列に格納
        seiseki[examNum][cnt] = buff;
        // 成績カウンタを1増やす
        cnt++;

        // 一人分の成績(13回)書き込んだら受験番号を増やす
        if (cnt > 12) {
            // 点数の合計を計算して配列に書き込み
            for (int i = 1; i < 13; i++)
                // テストの点数の和
                if (i < 4) seiseki[examNum][13] += seiseki[examNum][i];
                // 中学校の成績の和
                else {
                    // 記録点
                    seiseki[examNum][14] += seiseki[examNum][i];
                    // 理数バイアス付き記録点
                    seiseki[examNum][15] += (i == 6 || i == 7)
                                                ? (seiseki[examNum][i] * 1.4)
                                                : seiseki[examNum][i];
                }

            // 700点換算
            seiseki[examNum][15] *= (700.0 / 49.0);

            // 総合点 = 素点 + 記録換算点
            seiseki[examNum][16] = seiseki[examNum][13] + seiseki[examNum][15];

            cnt = 0;
            examNum++;
        }
    }

    // 人数分繰り返し
    for (int i = 1; i < 5; i++) {
        // 成績データの項目数の繰り返し
        for (int j = 0; j < 17; j++)
            // 成績データの書き込み
            fprintf(seisekiOut, "%.f\t", seiseki[i][j]);

        // 一人分書き込んだら改行
        fprintf(seisekiOut, "\n");
    }

    // ファイルを閉じる
    fclose(seisekiIn);
    fclose(seisekiOut);

    return 0;
}