#include <stdio.h>
#include <stdlib.h>

int main() {
    // 読み込む成績ファイル
    FILE *seisekiIn = fopen("seiseki.dat", "r");
    // 書き出す成績ファイル
    FILE *seisekiOut = fopen("seisekiOut.dat", "w");

    // 成績データ格納用2次元配列
    int seiseki[5][19] = {};

    // 一時格納用バッファ
    int buff = 0;
    // 成績カウンタ
    int cnt = 0;
    // 受験番号カウンタ
    int examNum = 1;

    // 成績データを一つずつファイルの終わりまで読み込む
    while (fscanf(seisekiIn, "%d", &buff) != EOF) {
        // 成績データを配列に格納
        seiseki[examNum][cnt] = buff;
        // 成績カウンタを1増やす
        cnt++;

        // 一人分の成績(13回)読んだら受験番号を増やす
        if (cnt > 12) {
            cnt = 0;
            examNum++;
        }
    }

    // ファイルを閉じる
    fclose(seisekiIn);
    fclose(seisekiOut);

    return 0;
}