#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
    //人数 + 1 の人数を記入
    int member = 5;
    //成績表
    int i;
    FILE *fp;
    char Line[member][512];  // http://wisdom.sakura.ne.jp/programming/c/c19.html
    char Line_v2[member][19][64];

    //成績ファイルから読み込み
    fp = fopen("Result.csv", "r");

    for (i = 1; i <= member; i++) {
        fgets(Line[i], 1000, fp);
    }

    fclose(fp);

    printf("Finish\n");
    fflush(stdout);
}
